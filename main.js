'use strict'

var deadline1 = new Date("2018-12-01T17:00:00").getTime()

setInterval(
function () {
  var now = new Date().getTime()
  var t = deadline1 - now
  var days = Math.floor(t / (1000 * 60 * 60 * 24))
  var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60))
  var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60))
  var seconds = Math.floor((t % (1000 * 60)) / 1000)
  document.getElementById('training').innerHTML =" in:  " + days + "d " 
  + hours + "h " + minutes + "m " + seconds + "s ";
}, 1000
)

